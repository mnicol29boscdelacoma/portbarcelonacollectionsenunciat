package cat.boscdelacoma.portbarcelona.views.ui.console;

import cat.boscdelacoma.portbarcelona.model.business.entities.Contenidor;
import cat.boscdelacoma.portbarcelona.model.business.entities.Mercaderia;
import cat.boscdelacoma.portbarcelona.model.business.entities.Vaixell;
import cat.boscdelacoma.portbarcelona.model.persistence.daos.impl.files.ContenidorFilesDAO;
import cat.boscdelacoma.portbarcelona.model.persistence.daos.impl.files.MercaderiaFilesDAO;
import cat.boscdelacoma.portbarcelona.model.persistence.exceptions.DAOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class PortBarcelona {

    public static void main(String[] args) {

        // crear instàncies de les classes que permeten accedir al sistema de 
        // persistència amb fitxers de dades
        var mercaderiaDAO = new MercaderiaFilesDAO();
        var contenidorDAO = new ContenidorFilesDAO();

        // crear una nova instància de vaixell
        var vaixell = new Vaixell();

        try {
            // obtenir tots els contenidors emmagatzemats
            var totsContenidors = contenidorDAO.getContenidors();
            // obtenir tres contenidors de la llista
            var c1 = totsContenidors.get("D9873456");
            var c2 = totsContenidors.get("C4562345");
            var c3 = totsContenidors.get("R7638493");

            // obrir el contenidor per poder afegir-hi mercaderies
            c1.obrir();
            afegirMercaderies(c1, mercaderiaDAO.getMercaderiesFromContainer(c1));
            c1.tancar();
            // carregar el contenidor al vaixell
            vaixell.addContenidor(c1);

            c2.obrir();
            afegirMercaderies(c2, mercaderiaDAO.getMercaderiesFromContainer(c2));
            c2.tancar();
            vaixell.addContenidor(c2);

            c3.obrir();
            afegirMercaderies(c3, mercaderiaDAO.getMercaderiesFromContainer(c3));
            c3.tancar();
            vaixell.addContenidor(c3);

            System.out.println("MOSTRAR CONTENIDORS VAIXELL");
            mostrarContenidors(vaixell);
            System.out.println("-------------------------------------------------");
            System.out.println("DESCARREGAR VAIXELL");
            descarregarVaixell(vaixell);
        } catch (DAOException ex) {
            System.out.println(ex.getMessage());
        }

    }

    static void afegirMercaderies(Contenidor contenidor, List<Mercaderia> mercaderies) {
        if (mercaderies != null) {
            for (var mercaderia : mercaderies) {
                contenidor.addMercaderia(mercaderia);
            }
        }
    }

    static void descarregarVaixell(Vaixell vaixell) {
        var data = LocalDate.now();
        var format = DateTimeFormatter.ofPattern("dd/MM/yyyy");

        System.out.println("PORT DE BARCELONA\n");
        System.out.printf("DATA: %s\n", data.format(format));
        System.out.println("CONTENIDOR         VOLUM");
        System.out.println("------------------------");
        Contenidor contenidor;

        for (var i = 0; i < vaixell.getnContenidors(); i++) {
            contenidor = vaixell.getContenidor(i);
            System.out.printf("%s %12.2f m3\n", contenidor.getNumSerie(), contenidor.getVolum());
        }
        System.out.printf("\nVOLUM TOTAL: %8.2f m3\n", vaixell.getVolum());
        System.out.println();
        vaixell.descarregar();
    }

    static void mostrarContenidors(Vaixell vaixell) {
        Contenidor contenidor;
        var countMercaderies = 0;

        for (var i = 0; i < vaixell.getnContenidors(); i++) {
            contenidor = vaixell.getContenidor(i);
            System.out.printf("%s\n", contenidor.getNumSerie());
            countMercaderies += contenidor.getnMercaderies();
        }
        System.out.printf("\nTOTAL MERCADERIES: %d\n", countMercaderies);
    }

}
