package cat.boscdelacoma.portbarcelona.model.business.entities;

public class Vaixell {

    public static final int MAX_CONTENIDORS = 1000;
    
    private Contenidor[] contenidors;
    private int nContenidors;
    private float volumOcupat;

    public Vaixell() {
        this.contenidors = new Contenidor[MAX_CONTENIDORS];
        this.nContenidors = 0;
        this.volumOcupat = 0;
    }

    public Vaixell(int nContenidors, float volumOcupat) {
        this();
        this.nContenidors = nContenidors;
        this.volumOcupat = volumOcupat;
    }

    
    /**
     * Afegeix un contenidor en el  vaixell.
     * 
     * Pot generar excepcions en cas que no es compleixin les restriccions indicades.
     * @param contenidor el contenidor que es vol afegir.
     */
    public void addContenidor(Contenidor contenidor) throws ArrayIndexOutOfBoundsException, UnsupportedOperationException  {

        if (nContenidors == MAX_CONTENIDORS) {
            // el contenidor està ple
            // TODO: què ha de fer en cas que el vaixell tingui el màxim de vaixells?
        } else if (!contenidor.isValid()) {
            // el contenidor no es pot carregar => inspecció DUANA
            // TODO: què ha de fer en cas que el contenidor no passi la inspecció de la DUANA?
        } else if (contenidor.getnMercaderies() == 0) {
            // el contenidor no té mercaderies
            // TODO: què ha de fer en cas que el contenidor que es vol carrgar no tingui mercaderies?
        } else if (contenidor.isObert()) {
            // el contenidor està  obert
            // TODO: què ha de fer si el contenidor està obert?
        } else {
            this.contenidors[this.nContenidors] = contenidor;
            this.nContenidors++;

            // acumular el volum del contenidor
            this.volumOcupat += contenidor.getVolum();
        }
    }

    /**
     * Obtenir el volum ocupat per les mercaderies dels contenidors del vaixell
     * @return obtenir el volum ocupat per les mercaderies dels contenidors del vaixell
     */
    public float getVolum() {
        return volumOcupat;
    }

    /**
     * Obetnir el contenidor indicat.
     * @param position la posició del contenidor que es vol obtenir
     * @return el contenidor a que s'ha demanat.
     */
    public Contenidor getContenidor(int position) {
        if(position >= 0 && position < nContenidors) {
            return contenidors[position];
        } else {
            // TODO: què ha de fer en cas de no trobar el contenidor?          
            return null;
        }
    }

    public int getnContenidors() {
        return nContenidors;
    }
    
    public Contenidor[] getContenidors() {
        return this.contenidors;
    }
    
    
    
    /**
     * Descarrega el vaixell
     */
    public void descarregar() {
        this.nContenidors = 0;
        this.volumOcupat = 0;
    }

}
