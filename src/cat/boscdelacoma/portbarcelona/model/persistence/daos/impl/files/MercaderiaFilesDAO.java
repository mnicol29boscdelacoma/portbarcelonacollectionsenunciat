package cat.boscdelacoma.portbarcelona.model.persistence.daos.impl.files;

import cat.boscdelacoma.portbarcelona.model.business.entities.Contenidor;
import cat.boscdelacoma.portbarcelona.model.business.entities.Mercaderia;
import cat.boscdelacoma.portbarcelona.model.persistence.daos.contracts.MercaderiaDAO;
import cat.boscdelacoma.portbarcelona.model.persistence.exceptions.DAOException;
import java.util.List;

/**
 * Classe que implementa un sistema de persistència d'objectes Mercaderia en 
 * fitxers orientats a byte
 * 
 * @author Marc Nicolau
 */
public class MercaderiaFilesDAO implements MercaderiaDAO {

    private final static String FILE_NAME = "mercaderies.dat";

    @Override
    public List<Mercaderia> getMercaderies() throws DAOException {
        throw new UnsupportedOperationException("Encara no s'ha implementat");
    }

    @Override
    public List<Mercaderia> getMercaderiesFromContainer(Contenidor container) throws DAOException {
        throw new UnsupportedOperationException("Encara no s'ha implementat");        
    }

    @Override
    public void saveMercaderies(List<Mercaderia> mercaderies) throws DAOException {
        throw new UnsupportedOperationException("Encara no s'ha implementat");
    }

}
